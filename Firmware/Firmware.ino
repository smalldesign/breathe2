/* #Breathe2 device to www.thingspeak.com firmware
   -----------------------------------------------
   Code created by Abhijit Savant. - 25th April 2019

   Objectives:
    - Read BME280 and SPS
    - Send data through SIM800L GPRS/GSM module equipped with Vodaphone 2G SIM card

   Modified 5th June 2019 - Modifications:
    1) SPS30 is never given the STOP measurement command
    2) All SPS30 values are reported including mass and number concentrations - boxcar average over 100 points per 5s.
    3) BME280 is also averaged as above.
    4) Values reported to 3 different channels on thingspeak.

   Modified 8th June 2019:
    1) Increase time per SIM800 command from 1s to 3s so that communicaiton could be more sure and regular.
    2) Take SPS30 values every 3s and record for 5 minutes.
    3) Get the maximum of PM0.5 values and in corresponding time slot, take all the rest.

   Modified for V1.2 PCBs - July 27
    1) Using median instead of maxima.
    2) Using FastRunningMedian library.
    3) Changes in some pins on new PCB.

   Modified - 24st Aug 2019
    1) "ESP.wdtFeed()" added everywhere, since yield() or delay() was not resetting the watchdogs apparently.

   Modified - 13th September 2019
    1) Using Sensirion's SPS30 library
    2) Converting from Median to average
    3) Taking differences in PM number concentrations, and then average of those diff for spectrum analysis.
    3) Sending floats instead of integers.

   Modified - 20th Sept. 2019
    1) From averaging to 'mode'
    2) Sampling is interrupt driven, independent of 'loop()'.

   Modified - 6th November 2019
    1) Make more generic and easy to change for different devices
    2) Make code format organized and readable (polishing)
*/

// Debug declarations
//------------------------------------------------------------------------------------------
// from : https://forum.arduino.cc/index.php?topic=215334.0
// uncomment this to print to Serial.
//#define debug

#ifdef debug
#define DEBUG_PRINT(...) Serial.print(__VA_ARGS__)
#define DEBUG_PRINTLN(...) Serial.println(__VA_ARGS__)
#else
#define DEBUG_PRINT(...)
#define DEBUG_PRINTLN(...)
#endif

// Wire.h for I2C communications (BME280, RTC, and SPS30)
//------------------------------------------------------------------------------------------
#include <Wire.h>


// function to delay without causing Wemos to reset
//--------------------------------------------------------------------
void long_delay(unsigned long period) {
  unsigned long loop_start_time = millis();
  unsigned long prev_time = millis();
  while ( millis() - loop_start_time < period)  {
    //yield();
    ESP.wdtFeed();
    if (millis() - prev_time > 1000) {
      ESP.wdtFeed();

      DEBUG_PRINT(".");
      //DEBUG_PRINT(system_get_free_heap_size());
      prev_time = millis();
    }
  }
  DEBUG_PRINTLN("o");
}


// Cloud transmission timing related
//----------------------------------------------------------------------------------------------
unsigned long transmission_frequency = 300000;   // Every 5 minutes
unsigned long last_data_transmission_time =  millis();
//bool first_run = true;

bool data_update_status = false;


//----------------------------------------------- Boxcar 'mode' --------------------------
// Store 100 data points of SPS30 and BME280 data to find maximum @3s interval = 300s = 5 minutes.
const int array_length = 100;
int array_index = 0;
// Record data every 3s.
const int sampling_frequency = 3;
unsigned long last_data_read_time =  millis();



void setup() {
  // ESP watchdog enabling to prevent unwanted resets due to default auto-watchdog configuration.
  ESP.wdtFeed();
  ESP.wdtEnable(15);

  /* Define baud rate for serial communication */
  Serial.begin(115200);

  DEBUG_PRINTLN(F("Testing..."));

  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, HIGH);

  // Initialize GPRS/GSM modem SIM800L
  init_SIM800();

  // Initialize P,T and RH sensor BME280
  init_BME280();

  // Initialize particulate matter sensor SPS30
  init_SPS30();

  // Initialize data sampling using interrupt timer
  init_data_sampling_timer();

  DEBUG_PRINTLN(F("init complete"));
}


void loop() {
  ESP.wdtFeed();

  // ----------------------- Transmission section -------------------
  // If 5 minutes are up then use SIM800 to send data to thingspeak.
  if (( millis() - last_data_transmission_time >= transmission_frequency ) || (millis() < last_data_transmission_time)) {
    Breathe2_SIM800_thingspeak();
    DEBUG_PRINT(F("Stats mode: "));
    DEBUG_PRINTLN(stats_mode(PM1_ugm3_array));
    last_data_transmission_time = millis();
  }
}

