

// For interrupt based sampling.
//----------------------------------------------------------------------------------------------
// ref: https://github.com/esp8266/Arduino/blob/master/libraries/Ticker/examples/TickerBasic/TickerBasic.ino
// from ref: https://forum.arduino.cc/index.php?topic=493258.0
#include <Ticker.h>

// Setup a timer.
Ticker data_sampling_timer;




// Initialize the ISR triggred by timer interrupt
//----------------------------------------------------------------------------------------------
void init_data_sampling_timer(void) {
  data_sampling_timer.attach(sampling_frequency, sample_sensors);
}

// Function that samples the BME280 and the SPS30
//----------------------------------------------------------------------------------------------
// and stores them into finite arrays. Array members are cycled repeatedly. No overflows.
void sample_sensors() {

  // SPS30
  digitalWrite(BUILTIN_LED, LOW);
  
  read_SPS30(array_index);

  unsigned long LED_on_time = millis();
  while (millis() - LED_on_time < 100) {}
  digitalWrite(BUILTIN_LED, HIGH);

  LED_on_time = millis();
  while (millis() - LED_on_time < 100) {}

  // BME280
  digitalWrite(BUILTIN_LED, LOW);
  read_BME280(array_index);
  
  LED_on_time = millis();
  while(millis()-LED_on_time < 100) {}
  
  digitalWrite(BUILTIN_LED, HIGH);

  // Increment the index
  array_index++;

  // If its equal to the length of boxcar then reset it
  if ( array_index == array_length ) array_index = 0;
}






